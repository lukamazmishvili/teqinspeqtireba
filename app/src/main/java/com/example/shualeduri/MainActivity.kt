package com.example.shualeduri

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import androidx.core.text.isDigitsOnly

class MainActivity : AppCompatActivity() {

    private lateinit var firstAB : EditText
    private lateinit var secondAB : EditText
    private lateinit var techNumBox : EditText
    private lateinit var digits : EditText
    private lateinit var checkButt : Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        firstAB = findViewById(R.id.firstAB)
        secondAB = findViewById(R.id.secondAB)
        techNumBox = findViewById(R.id.techNumBox)
        checkButt = findViewById(R.id.checkButt)
        digits = findViewById(R.id.digits)


        checkButt.setOnClickListener {

            val ab = firstAB.text.toString().trim()
            val nums = digits.text.toString().trim()
            val ab1 = secondAB.text.toString().trim()
            val techNum = techNumBox.text.toString().trim()


            if (firstAB.length() != 2) {

                firstAB.error = "არასწორია*"
                return@setOnClickListener

            } else if (ab.isEmpty()) {

                firstAB.error = "შეიყვანეთ ნომერი*"
                return@setOnClickListener

            } else if (ab.isDigitsOnly()) {

                firstAB.error = "შეიყვანეთ ასოები*"
                return@setOnClickListener

            } else if (ab.isEmpty()) {

                firstAB.error = "არასწორია*"
                return@setOnClickListener

            }else if (techNum.length != 9) {

                techNumBox.error = "არასწორია*"
                return@setOnClickListener

            }else if (techNum.isEmpty()) {

                techNumBox.error = "არასწორია*"
                return@setOnClickListener

            }else if (ab1.length != 2) {

                secondAB.error = "არასწორია*"
                return@setOnClickListener

            }  else if (ab1.isEmpty()) {

                secondAB.error = "არასწორია*"
                return@setOnClickListener

            }else if (nums.length != 3 ){

                digits.error = "არასწორია*"
                return@setOnClickListener

            }else if (!nums.isDigitsOnly()) {

                digits.error = "არასწორია*"
                return@setOnClickListener

            }else if (nums == "000") {

                digits.error = "არასწორია*"
                return@setOnClickListener

            }else

                Toast.makeText(this, "თქვენ გავლილი გაქვთ ტექ-დათვალიერება", Toast.LENGTH_SHORT).show()

        }
    }
}